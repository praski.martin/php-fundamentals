    <?php
//Task 1: Multiply

//Multiply the variables

$a =2;
$b =4;
function multiply($a, $b) {
    return $a * $b;
}
echo 'Result of task 1: '.multiply($a,$b).'<br>';

//Task 2: Opposite number

//Given a number, find its opposite.

$n =-2;
function opposite($n) {
    if($n > 0){
        return ('-'.$n);
    }else if($n < 0){
        return (abs($n));
    }else{
        return $n;
    }
}
echo 'Result of task 2: '.opposite($n).'<br>';

//Task 3: Replace With Alphabet Position

//You are required to, given a string, replace every letter with its position in the alphabet.
//If anything in the text isn't a letter, ignore it and don't return it.
//"a" = 1, "b" = 2, etc.
//Example:
//alphabet_position('The sunset sets at twelve o\' clock.');
//Should return "20 8 5 19 21 14 19 5 20 19 5 20 19 1 20 20 23 5 12 22 5 15 3 12 15 3 11" (as a string)

$s= 'The sunset sets at twelve o\' clock.';
function alphabet_position(string $s): string {
    $s = str_replace(' ', '', $s);  //usuwa spacje
    $s = str_replace('\\', '',$s); //usuwa  \
    $s = str_replace('.', '',$s); //usuwa  \
    $s = str_replace("'", '',$s); //usuwa  \
    $s = strtolower($s); //zmiana wielkosci liter
    $s = str_split($s);  //robi tablice
    $asoc = ['a' => 1,
        'b' => 2,
        'c' => 3,
        'd'  => 4,
        'e'  => 5,
        'f'  => 6,
        'g'  => 7,
        'h'  => 8,
        'i'  => 9,
        'j'  => 10,
        'k'  => 11,
        'l'  => 12,
        'm'  => 13,
        'n'  => 14,
        'o'  => 15,
        'p'  => 16,
        'q'  => 17,
        'r'  => 18,
        's'  => 19,
        't'  => 20,
        'u'  => 21,
        'v'  => 22,
        'w'  => 23,
        'x'  => 24,
        'y'  => 25,
        'z'  => 26,
    ];
    $result = '';
    foreach ($s as $v) {
        foreach ($asoc as $key => $value){
            if ($v == $key) {
                $result .= $value.' ';
            }
        }
    }
    return  $s= substr($result, 0, -1);
}
echo 'Result of task 3: '.alphabet_position($s).'<br>';

//Task 4: Reversed Strings

//Complete the solution so that it reverses the string value passed into it.
//solution("world"); // returns "dlrow"

$str= 'world';
function solution($str) {
    return strrev($str) ;
}
echo 'Result of task 4: '.solution($str).'<br>';

//Task 5: Remove First and Last Character

//Your goal is to create a function that removes the first and last characters of a string.
//You're given one parameter, the original string. You don't have to worry with strings with less than two characters.

$s= 'eloquent';
function remove_char(string $s): string {
    return $s = substr($s,1,-1);
}
echo 'Result of task 5: '.remove_char($s).'<br>';

//Abbreviate a Two Word Name

//Write a function to convert a name into initials. This task strictly takes two words with one space in between them.
//The output should be two capital letters with a dot separating them
//It should look like this:
//Sam Harris => S.H
//Patrick Feeney => P.F

$name='Sam Harris';
function abbrevName($name)
{
    $nameTable = explode(' ', $name);
    $name = strtoupper(substr($nameTable[0],0,1).'.'.substr($nameTable[1],0,1));
    return $name;
}
echo 'Result of task 6:'.abbrevName($name).'<br>';

//Switch it Up!

//When provided with a number between 0-9, return it in words.
//Input :: 1
//Output :: "One".
//If your language supports it, try using a switch statement.

$number= 2;
function switch_it_up($number)
{
    switch ($number){
        case '0':
            return 'Zero';
            break;
        case '1':
            return 'One';
            break;
        case '2':
            return 'Two';
            break;
        case '3':
            return 'Three';
            break;
        case '4':
            return 'Four';
            break;
        case '5':
            return 'Five';
            break;
        case '6':
            return 'Six';
            break;
        case '7':
            return 'Seven';
            break;
        case '8':
            return 'Eight';
            break;
        case '9':
            return 'Nine';
            break;
    }
}
echo 'Result of task 7:'.switch_it_up($number).'<br>';

//Find the stray number

//You are given an odd-length array of integers, in which all of them are the same, except for one single number.
//Complete the method which accepts such an array, and returns that single different number.
//The input array will always be valid! (odd-length >= 3)
//Examples
//[1, 1, 2] ==> 2
//[17, 17, 3, 17, 17, 17, 17] ==> 3

$arr=[12,12,12,6,12,12];
function stray($arr){
    if($arr[0]!=$arr[1]){
        if($arr[1]===$arr[2]){
            return $arr[0];
        }elseif($arr[0]===$arr[2]){
            return $arr[1];
        }
    }else{
        foreach ($arr as $v){
            if($v != $arr[0]){
                return $v;
            }
        }
    }
}
echo 'Result of task 8:'.stray($arr).'<br>';

//Kebabize

//Modify the kebabize function so that it converts a camel case string into a kebab case.
//kebabize('camelsHaveThreeHumps') // camels-have-three-humps
//kebabize('camelsHave3Humps') // camels-have-humps
//Notes:
//the returned string should only contain lowercase letters

$string = 'camelsHaveThreeHumps';
function kebabize($string) {
    $result='';
    foreach (str_split($string) as $val){
        if(is_string($val)){
            if(ctype_upper($val)){
                $result.='-'.strtolower($val);
            }elseif(ctype_lower($val)){
                $result.=$val;
            }
        }
    }
    if(substr($result,0,1) == '-'){
        return substr($result, 1);
    }else{
        return $result;
    }
}
echo 'Result of task 9:'.kebabize($string).'<br>';

//Fix string case

/*
You will be given a string that may have mixed uppercase and lowercase
letters and your task is to convert that string to either lowercase only or uppercase only based on:
make as few changes as possible.
if the string contains equal number of uppercase and lowercase letters, convert the string to lowercase.
For example:
solve("coDe") = "code". Lowercase characters > uppercase. Change only the "D" to lowercase.
solve("CODe") = "CODE". Uppercase characters > lowecase. Change only the "e" to uppercase.
solve("coDE") = "code". Upper == lowercase. Change all to lowercase.
 */

$s = 'coDe';
function solve($s)
{
    $lowercase = 0;
    $uppercase = 0;
    foreach (str_split($s) as $v) {
        if (ctype_lower($v)) {
            $lowercase++;
        } else {
            $uppercase++;
        }
    }
    if ($uppercase <= $lowercase) {
        return strtolower($s);
    }else{
        return strtoupper($s);
    }
}
echo 'Result of task 10:'.solve($s).'<br>';

//Find the odd int

/*
 * Given an array, find the integer that appears an odd number of times.
There will always be only one integer that appears an odd number of times.
 */

$seq = [20,1,-1,2,-2,3,3,5,5,1,2,4,20,4,-1,-2,5];
function findIt(array $seq) : int
{
    $count=0;
    foreach ($seq as $val){
        foreach ($seq as $val2){
            if($val==$val2){
                $count++;
            }
        }
        if ($count % 2 != 0){
            return $val;
        }
    }
}
echo 'Result of task 11:'.findIt($seq).'<br>';

//Detect Pangram

/*
 * A pangram is a sentence that contains every single letter of the alphabet at least once.
 * For example, the sentence "The quick brown fox jumps over the lazy dog" is a pangram,
 * because it uses the letters A-Z at least once (case is irrelevant).
Given a string, detect whether or not it is a pangram. Return True if it is, False if not. Ignore numbers and punctuation.
 */

$string= 'The quick brown fox jumps over the lazy dog.';  //true
//$string= '5B!e i J x*p F h d!A:o q D y n6L%u9i.G9f2g4C a h+K!m+z:R t!j:B w s C';  //false
function detect_pangram($string) {
    $arr = str_split($string);
    $alphabet = range('a','z');
    foreach ($arr as $val){
        if(ctype_alpha($val)){
            if (ctype_upper($val)) {
                $val = strtolower($val);
            }
            $key = array_search($val, $alphabet);
            if ($key !== false) {
                unset($alphabet[$key]);
            }
        }
    }
    $result = false;
    if (!$alphabet) {
        $result = true;
    }
    return $result;
}
echo 'Result of task 12:'.detect_pangram($string).'<br>';

//Descending Order

/*
 * Your task is to make a function that can take any non-negative integer as a argument
 * and return it with its digits in descending order.
 * Essentially, rearrange the digits to create the highest possible number.
 * Examples:
Input: 21445 Output: 54421
Input: 145263 Output: 654321
Input: 123456789 Output: 987654321
 */

$n= 123456789;
function descendingOrder(int $n): int {
    if(is_int($n) > 0){
        $n= str_split($n);
        rsort($n);
    }
    $result= 0;
    foreach($n as $val){
        $result.= $val;
        $result = (int)$result;
    }
    return $result;
}
echo 'Result of task 13:'.descendingOrder($n).'<br>';

//Help the Fruit Guy

/*
 * Our fruit guy has a bag of fruit (represented as an array of strings) where some fruits are rotten.
He wants to replace all the rotten pieces of fruit with fresh ones.
For example, given ["apple","rottenBanana","apple"] the replaced array should be ["apple","banana","apple"].
Your task is to implement a method that accepts an array of strings containing fruits should returns an array of strings
where all the rotten fruits are replaced by good ones.
Notes
If the array is null/nil/None or empty you should return empty array ([]).
The rotten fruit name will be in this camelcase (rottenFruit).
The returned array should be in lowercase.
 */

$fruitBasket= ["apple","rottenBanana","kiwi","rottenMango"];
function removeRotten($fruitBasket) {
    $freshFruit= [];
    if(!$fruitBasket){
        return $freshFruit;
    }
    foreach ($fruitBasket as $fruit){
        $fruit= str_replace("rotten","",$fruit);
        $fruit= strtolower($fruit);
        $freshFruit[].= $fruit;
    }
    return $freshFruit;
}

function show($freshFruit){
    $fruits='';
    foreach ($freshFruit as $fruit){
        $fruits.= $fruit.' ';
    }
    return $fruits;
}
echo 'Result of task 14:'.show(removeRotten($fruitBasket)).'<br>';

//Build a square

//I will give you an integer. Give me back a shape that is as long and wide as the integer.
//The integer will be a whole number between 0 and 50.
//Example
//n = 3, so I expect a 3x3 square back just like below as a string:
//+++
//+++
//+++

$int=5;
function generateShape($int) {
    $multiply=$int*$int;
    $result="";
    for ($i= 1; $i <= $multiply; $i++){
        $result.= "+";
        if($i != 0){
            if(0===($i % $int) && $multiply !=$i){
                $result.= "<br>";
            }
        }
    }
    return $result;
}
echo 'Result of task 15:'.generateShape($int).'<br>';

//Vowel Count

//Return the number (count) of vowels in the given string.
//We will consider a, e, i, o, and u as vowels for this task.
//The input string will only consist of lower case letters and/or spaces.

$str= "abracadabra";
function getCount($str) {
    $arr=str_split($str);
    $vowelsCount = 0;
    foreach ($arr as $letter){
        if ($letter ==='a' || $letter === 'e' || $letter === 'i' || $letter === 'o' || $letter === 'u'){
            $vowelsCount++;
        }
    }
    return $vowelsCount;
}
echo 'Result of task 16:'.getCount($str).'<br>';

//Shortest Word

//Simple, given a string of words, return the length of the shortest word(s).
//String will never be empty and you do not need to account for different data types.

$str= "bitcoin take over the world maybe who knows perhaps";
function findShort($str){
    $words = explode(" ", $str);
    $shortestWord = strlen($words[0]);
    foreach ($words as $word) {
        if (strlen($word) < $shortestWord) {
            $shortestWord = strlen($word);
        }
    }
    return $shortestWord;
}
echo 'Result of task 17:'.findShort($str).'<br>';

//Mumbling

//This time no story, no theory. The examples below show you how to write function accum:
//accum("abcd") -> "A-Bb-Ccc-Dddd"
//accum("RqaEzty") -> "R-Qq-Aaa-Eeee-Zzzzz-Tttttt-Yyyyyyy"
//accum("cwAt") -> "C-Ww-Aaa-Tttt"
//The parameter of accum is a string which includes only letters from a..z and A..Z.

$s='ZpglnRxqenU';
function accum($s) {
    $s=strtolower($s);
    $letters= str_split($s);
    $arrayLength= count($letters);
    $characterNumber= 0;
    $result='';
    foreach ($letters as $letter){
        $characterNumber++;
        $transformedLetter='';
        for($i=$arrayLength-$characterNumber ; $i< $arrayLength; $i++){
            $transformedLetter.= $letter;
        }
        $transformedLetter=ucwords($transformedLetter);
        $result.= $transformedLetter.'-';
    }
    $result= substr($result, 0, -1);
    return $result;
}
echo 'Result of task 18:'.accum($s).'<br>';

//Special Number (Special Numbers Series #5)

//A number is a Special Number if it’s digits only consist 0, 1, 2, 3, 4 or 5
//Given a number determine if it special number or not .
//The number passed will be positive (N > 0) .
//All single-digit numbers with in the interval [0:5] are considered as special number.
//Input >> Output Examples
//specialNumber(2) ==> return "Special!!"
//Explanation:
//It's a single-digit number within the interval [0:5] .
//specialNumber(9) ==> return "NOT!!"
//Explanation:
//Although, it's a single-digit number but Outside the interval [0:5] .
//specialNumber(23) ==> return "Special!!"
//Explanation:
//All the number's digits formed from the interval [0:5] digits .
//specialNumber(39) ==> return "NOT!!"
//Explanation:
//Although, there is a digit (3) Within the interval But the second digit is not (Must be ALL The Number's Digits ) .
//specialNumber(59) ==> return "NOT!!"
//Explanation:
//Although, there is a digit (5) Within the interval But the second digit is not (Must be ALL The Number's Digits ) .
//specialNumber(513) ==> return "Special!!"
//specialNumber(709) ==> return "NOT!!"

$n= 45;
function specialNumber($n) {
    $arrayOfNumbers= str_split($n);
    $special=0;
    foreach ($arrayOfNumbers as $number){
        $number=(int) $number;
        if($number >= 0 && $number <=5){
            $special++;
        }
    }
    if ($special === count($arrayOfNumbers)){
        return 'Special!!';
    }else{
        return 'NOT!!';
    }
}
echo 'Result of task 19:'.specialNumber($n).'<br>';

//String ends with?

//Complete the solution so that it returns true if the first argument(string) passed in ends with the 2nd argument (also a string).
//Examples:
//solution('abc', 'bc') // returns true
//solution('abc', 'd') // returns false

$str='abcabc';
$ending='bc';
function stringEndsWith($str, $ending) {
    $lengthEnding= strlen($ending);
    $lengthStr= strlen($str);
    if($ending=== substr($str,($lengthStr-$lengthEnding))){
        return true;
    }else{
        return false;
    }
}
echo 'Result of task 20:'.stringEndsWith($str,$ending).'<br>';

//Product of Array Items

//Calculate the product of all elements in an array.
//If the array is NULL or empty, return NULL.

$a=[5,8];
function product($a) {
    if(empty($a)){
        return null;
    }else{
        $result=1;
        foreach ($a as $v){
            $result*= $v;
        }
        return $result;
    }
}
echo 'Result of task 21:'.product($a).'<br>';

//Put the exclamation marks and question marks to the balance, Are they balanced?

//Description:
//Each exclamation mark weight is 2; Each question mark weight is 3.
//Put two string left and right to the balance, Are they balanced?
//If the left side is more heavy, return "Left"; If the right side is more heavy, return "Right";
//If they are balanced, return "Balance".
//    Examples
//balance("!!","??") == "Right"
//balance("!??","?!!") == "Left"
//balance("!?!!","?!?") == "Left"
//balance("!!???!????","??!!?!!!!!!!") == "Balance"

$l="!!???!????";
$r="??!!?!!!!!!!";
function balance(string $l, string $r): string {
    $arrL= str_split($l);
    $arrR= str_split($r);
    $resultL=0;
    $resultR=0;
    foreach($arrL as $vL){
        if($vL=='!'){
            $resultL+=2;
        }else{
            $resultL+=3;
        }
    }
    foreach($arrR as $vR){
        if($vR=='!'){
            $resultR+=2;
        }else{
            $resultR+=3;
        }
    }
    if($resultL > $resultR){
        return 'Left';
    }elseif($resultL < $resultR){
        return 'Right';
    }else{
        return 'Balance';
    }
}
echo 'Result of task 22:'.balance($l,$r).'<br>';

//Spacify

//Modify the spacify function so that it returns the given string with spaces inserted between each character.
//spacify("hello world") // "h e l l o   w o r l d"

$s='hello world';
function spacify(string $s): string {
    $s=str_split($s);
    $result='';
    foreach($s as $v){
        $result.=$v.' ';
    }
    $result= substr($result,0,-1);
    return $result;
}
echo 'Result of task 23:'.spacify($s).'<br>';

//Small enough? - Beginner

//You will be given an array and a limit value. You must check that all values in the array are below or equal to the limit value.
//If they are, return true. Else, return false.
//    You can assume all values in the array are numbers.

$a=[78, 117, 110, 99, 104, 117, 107, 115];
$limit= 100;
function smallEnough($a, $limit)
{
    foreach ($a as $v){
        if($v > $limit){
            return false;
        }
    }
    return true;
}
echo 'Result of task 24:'.smallEnough($a,$limit).'<br>';

//Reverse Letters in Sentence

//Take a sentence (string) and reverse each word in the sentence.
//Do not reverse the order of the words, just the letters in each word.
//If there is punctuation, it should be interpreted as a regular character; no special rules.
//If there is spacing before/after the input string, leave them there.
//String will not be empty.
//Examples
//"Hi mom" => "iH mom"
//" A fun little challenge! " => " A nuf elttil !egnellahc "

$sentence="A fun little challenge";
function reverser(string $sentence): string {
    $words = explode(' ',$sentence);
    $result= '';
    foreach($words as $word){
        $result.=strrev($word).' ';
    }
    return $result=substr($result,0,-1);
}
echo 'Result of task 25:'.reverser($sentence).'<br>';

//Count the smiley faces!

//Given an array (arr) as an argument complete the function countSmileys that should return the total number of smiling faces.
//Rules for a smiling face:
//    -Each smiley face must contain a valid pair of eyes. Eyes can be marked as : or ;
//-A smiley face can have a nose but it does not have to. Valid characters for a nose are - or ~
//-Every smiling face must have a smiling mouth that should be marked with either ) or D.
//No additional characters are allowed except for those mentioned.
//Valid smiley face examples:
//:) :D ;-D :~)
//Invalid smiley faces:
//;( :> :} :]
//Example cases:
//countSmileys([':)', ';(', ';}', ':-D']);       // should return 2;
//countSmileys([';D', ':-(', ':-)', ';~)']);     // should return 3;
//countSmileys([';]', ':[', ';*', ':$', ';-D']); // should return 1;
//Note: In case of an empty array return 0. You will not be tested with invalid input (input will always be an array).
//Order of the face (eyes, nose, mouth) elements will always be the same

$arr=[':D',':~)',';~D',':)'];
function count_smileys($arr): int {
    if(empty($arr)){
        return 0;
    }
    $correctAmount= 0;
    foreach($arr as $letters){
        if(($letters[0]=== ':' ||$letters[0]=== ';') && ($letters[1]=== '-' ||$letters[1]=== '~') && ($letters[2]===')' ||$letters[2]=== 'D' )){
            $correctAmount++;
        }elseif(($letters[0]=== ':' ||$letters[0]=== ';') && ($letters[1]=== ')' ||$letters[1]=== 'D')){
            $correctAmount++;
        }
    }
    return $correctAmount;
}
echo 'Result of task 26:'.count_smileys($arr).'<br>';

//Highest Scoring Word

//Given a string of words, you need to find the highest scoring word.
//Each letter of a word scores points according to its position in the alphabet: a = 1, b = 2, c = 3 etc.
//You need to return the highest scoring word as a string.
//If two words score the same, return the word that appears earliest in the original string.
//All letters will be lowercase and all inputs will be valid.

$x='man i need a taxi up to ubud';
function high($x) {
    $alphabet=array_combine(range('a','z'),range(1,26));
    $words=explode(' ',$x);
    $highestSum=0;
    $result='';
    foreach($words as $word){
        $letters=str_split($word);
        $sum=0;
        foreach($letters as $letter){
            foreach($alphabet as $key => $val){
                if($letter === $key){
                    $sum+=$val;
                }
            }
        }
        if($highestSum < $sum){
            $highestSum=$sum;
            $result=$word;
        }
    }
    return $result;
}
echo 'Result of task 27:'.high($x).'<br>';

//Given a number, return a string with dash'-'marks before and after each odd integer,
//but do not begin or end the string with a dash mark.
//Ex:
//dashatize(274) -> '2-7-4'
//dashatize(6815) -> '68-1-5'

$num = 974302;
function dashatize(int $num): string {
    $num = str_split( (string) $num );
    $result = '';
    foreach ($num as $digit){
        if($digit % 2){
            if(substr($result, -1) === '-'){
                $result.=$digit.'-';
            }else{
                $result.='-'.$digit.'-';
            }
        }else{
            $result.= $digit;
        }
    }
    if(substr($result,-1) === '-'){
        $result=rtrim($result, '-');
    }
    if(substr($result,0,1) === '-'){
        $result=ltrim($result, '-');
    }
    return $result;
}
echo 'Result of task 28:'.dashatize($num).'<br>';

//If you can read this...

//You'll have to translate a string to Pilot's alphabet (NATO phonetic alphabet) wiki.
//Like this:
//** Input: ** If you can read
//** Output: ** India Foxtrot Yankee Oscar Uniform Charlie Alfa November Romeo Echo Alfa Delta
//** Some notes **
//Keep the punctuation, and remove the spaces.
//Use Xray without dash or space.

$words = 'If you can read';
function to_nato($words){
    $phoneticAlphabetNATO=[
        'a'=>"Alfa",
        'b'=>"Bravo",
        'c'=>"Charlie",
        'd'=>"Delta",
        'e'=>"Echo",
        'f'=>"Foxtrot",
        'g'=>"Golf",
        'h'=>"Hotel",
        'i'=>"India",
        'j'=>"Juliet",
        'k'=>"Kilo",
        'l'=>"Lima",
        'm'=>"Mike",
        'n'=>"November",
        'o'=>"Oscar",
        'p'=>"Papa",
        'q'=>"Quebec",
        'r'=>"Romeo",
        's'=>"Sierra",
        't'=>"Tango",
        'u'=>"Uniform",
        'v'=>"Victor",
        'w'=>"Whiskey",
        'x'=>"Xray",
        'y'=>"Yankee",
        'z'=>"Zulu",
        '0'=>"Zero",
        '1'=>"One",
        '2'=>"Two",
        '3'=>"Three",
        '4'=>"Four",
        '5'=>"Five",
        '6'=>"Six",
        '7'=>"Seven",
        '8'=>"Eight",
        '9'=>"Nine",
        '-'=>"Dash"
    ];
    $words=str_split($words);
    $signs=['.','!','?','-'];
    $result='';
    foreach($words as $letter ){
        foreach ($phoneticAlphabetNATO as $character => $telephony){
            if(strtolower($letter) === $character){
                $result.=$telephony.' ';
            }
        }
        if($letter === $signs[0] || $letter === $signs[1] || $letter === $signs[2]){
            $result.=$letter.' ';
        }
    }
    $result=rtrim($result);
    return $result;
}
echo 'Result of task 29:'.to_nato($words).'<br>';

//Build Tower

//Build Tower by the following given argument:
//number of floors (integer and always greater than 0).
//Tower block is represented as *
//PHP: returns an array;
//for example, a tower of 3 floors looks like below
//[
//  '  *  ',
//  ' *** ',
//  '*****'
//]
//and a tower of 6 floors looks like below
//[
//  '     *     ',
//  '    ***    ',
//  '   *****   ',
//  '  *******  ',
//  ' ********* ',
//  '***********'
//]

$floors=3;
function tower_builder(int $floors): array
{
    $result=[];
    for ($i=0; $i<$floors; $i++)
    {
        $row= '';
        for ($j=0; $j<$floors-$i-1; $j++)
            $row .= "&nbsp;";
        for ($j=0; $j<=$i; $j++)
            $row .= "*";
        for ($j=0; $j<=$i-1; $j++)
            $row .= "*";
        for ($j=0; $j<$floors-$i-1; $j++)
            $row .= " ";
        $row .= "<br>";
        $result[]=$row;
    }
    return $result;
}
echo 'Result of task 30:'.'<br>';
foreach (tower_builder($floors) as $floor){
    echo "<span style='font-family: Courier New'>";
    echo $floor;
    echo "</span>";
}

//Statistics for an Athletic Association

//You are the "computer expert" of a local Athletic Association (C.A.A.).
//Many teams of runners come to compete. Each time you get a string of all race results of every team who has run.
//For example here is a string showing the individual results of a team of 5 runners:
//"01|15|59, 1|47|6, 01|17|20, 1|32|34, 2|3|17"
//Each part of the string is of the form: h|m|s where h, m, s (h for hour, m for minutes, s for seconds) are positive
//or null integer (represented as strings) with one or two digits. There are no traps in this format.
//
//To compare the results of the teams you are asked for giving three statistics; range, average and median.
//Range : difference between the lowest and highest values. In {4, 6, 9, 3, 7} the lowest value is 3, and the highest is 9,
//so the range is 9 − 3 = 6.
//Mean or Average : To calculate mean, add together all of the numbers in a set and
//then divide the sum by the total count of numbers.
//Median : In statistics, the median is the number separating the higher half of a data sample from the lower half.
//The median of a finite list of numbers can be found by arranging all the observations from lowest value to highest value
//and picking the middle one (e.g., the median of {3, 3, 5, 9, 11} is 5) when there is an odd number of observations.
//If there is an even number of observations, then there is no single middle value;
//the median is then defined to be the mean of the two middle values (the median of {3, 5, 6, 9} is (5 + 6) / 2 = 5.5).
//Your task is to return a string giving these 3 values. For the example given above, the string result will be
//"Range: 00|47|18 Average: 01|35|15 Median: 01|32|34"
//of the form:
//"Range: hh|mm|ss Average: hh|mm|ss Median: hh|mm|ss"
//where hh, mm, ss are integers (represented by strings) with each 2 digits.
//Remarks:
//if a result in seconds is ab.xy... it will be given truncated as ab.
//if the given string is "" you will return ""

$strg="01|15|59, 1|47|16, 01|17|20, 1|32|34, 2|17|17";
function statAssoc($strg) {
    if(empty($strg)){
        return"";
    }

    $dataTable = explode(" ", $strg);
    $time = [];
    foreach ($dataTable as $data){
        $data = rtrim($data,',');
        $data = explode("|",$data);
        $h = $data[0] * 3600;
        $m = $data[1] * 60;
        $time[]= $h + $m + $data[2];
    }
    $highestArrayValue = max($time);
    $lowestArrayValue = min($time);
    $range = $highestArrayValue - $lowestArrayValue;  // result
    $average = 0;
    foreach($time as $unit){
        $average += $unit;
    }

    $average = floor($average / count($time));  // result
    sort($time);
    $arrlength = count($time);
    if($arrlength % 2){
        $centerOfTheArr = $arrlength /2;
        $median= $time[$centerOfTheArr];
    }else{
        $centerOfTheArr = ($time[$arrlength /2] + $time[$arrlength /2 -1])/2;
        $median = $centerOfTheArr;    //result
    }

    $hoursR = $range / 3600.0;
    $hoursR = floor($hoursR);
    if(strlen($hoursR) === 1){
        $hoursR ='0'.$hoursR;
    }
    $minsR = ($range % 3600.0) / 60;
    $minsR = floor($minsR);
    if(strlen($minsR) === 1){
        $minsR ='0'.$minsR;
    }
    $secR = $range - (($hoursR * 3600)+($minsR * 60));
    if(strlen($secR) === 1){
        $secR ='0'.$secR;
    }

    $hoursA = $average / 3600.0;
    $hoursA = floor($hoursA);
    if(strlen($hoursA) === 1){
        $hoursA ='0'.$hoursA;
    }
    $minsA = ($average % 3600.0) / 60;
    $minsA = floor($minsA);
    if(strlen($minsA) === 1){
        $minsA ='0'.$minsA;
    }
    $secA = $average - (($hoursA * 3600)+($minsA * 60));
    if(strlen($secA) === 1){
        $secA ='0'.$secA;
    }

    $hoursM = $median / 3600.0;
    $hoursM = floor($hoursM);
    if(strlen($hoursM) === 1){
        $hoursM ='0'.$hoursM;
    }

    $minsM = ($median % 3600.0) / 60;
    $minsM = floor($minsM);
    if(strlen($minsM) === 1){
        $minsM ='0'.$minsM;
    }
    $secM = $median - (($hoursM * 3600)+($minsM * 60));
    $secM = floor($secM);
    if(strlen($secM) === 1){
        $secM ='0'.$secM;
    }
    if(strlen($secM) === 4){
        $secM = substr($secM,0,2);
    }

    return $result='Range: '.$hoursR.'|'.$minsR.'|'.$secR.' Average: '.$hoursA.'|'.$minsA.'|'.$secA.' Median: '.$hoursM.'|'.$minsM.'|'.$secM;
}
echo 'Result of task 31: '.statAssoc($strg).'<br>';

//TASK 32: Organise duplicate numbers in list

//Sam is an avid collector of numbers.
//Every time he finds a new number he throws it on the top of his number-pile.
//Help Sam organise his collection so he can take it to the International Number Collectors Conference in Cologne.
//Given an array of numbers, your function should return an array of arrays,
//where each subarray contains all the duplicates of a particular number.
//Subarrays should be in the same order as the first occurence of the number they contain:
//group([3, 2, 6, 2, 1, 3])
//>>> [[3, 3], [2, 2], [6], [1]]
//Assume the input is always going to be an array of numbers. If the input is an empty array, an empty array should be returned.

$arr=[3, 2, 6, 2, 1, 3];
function group(array $arr) {
    $grouped = [];
    foreach ($arr as $number){
        if(!isset($grouped[$number])){
            $grouped[$number] = [];
        }
        $grouped[$number][] = $number;
    }
    $grouped = array_values($grouped);
    return $grouped;
}

//TASK 33 Mexican Wave

//You will be passed a string and you must return that string in an array where an uppercase letter is a person standing up.
//Rules
//1.  The input string will always be lower case but maybe empty.
//2.  If the character in the string is whitespace then pass over it as if it was an empty seat.
//    Example
//wave("hello") => []string{"Hello", "hEllo", "heLlo", "helLo", "hellO"}

$people='hello';
function wave($people){
    $result = [];
    foreach (str_split($people) as $key => $val ){
        if($val === ' ' || $people ==''){
            continue;
        }
        $wave=$people;
        $wave[$key] = strtoupper($val);
        array_push($result,$wave);
    }
    return $result;
}

//TASK 34  Reverse every other word in the string

//Reverse every other word in a given string, then return the string.
//Throw away any leading or trailing whitespace, while ensuring there is exactly one space between each word.
//Punctuation marks should be treated as if they are a part of the word in this kata.

$str = 'Did it work?';
function reverse($str) {
    $arr= explode(' ',trim($str));
    $result='';
    foreach($arr as $number => $word){
        if($number % 2 === 1){
            $word = strrev($word);
        }
        $result.=$word.' ';
    }
    return trim($result);
}
echo 'Result of task 34: '.reverse($str).'<br>';

//Persistent Bugger.

//Write a function, persistence, that takes in a positive parameter num and returns its multiplicative persistence,
//which is the number of times you must multiply the digits in num until you reach a single digit.
//For example:
//persistence(39) === 3; // because 3 * 9 = 27, 2 * 7 = 14, 1 * 4 = 4 and 4 has only one digit
//persistence(999) === 4; // because 9 * 9 * 9 = 729, 7 * 2 * 9 = 126, 1 * 2 * 6 = 12, and finally 1 * 2 = 2
//persistence(4) === 0; // because 4 is already a one-digit number

$num = 999;
function persistence(int $num): int {
    $result=0;
    while(strlen($num) > 1){
        $num = array_product(str_split($num));
        $result++;
    }
    return $result;
}
echo 'Result of task 35: '.persistence($num).'<br>';

// TASK 36 Unique In Order

//Implement the function unique_in_order which takes as argument a sequence and
//returns a list of items without any elements with the same value next to each other and
//preserving the original order of elements.
//For example:
//uniqueInOrder("AAAABBBCCDAABBB") == {'A', 'B', 'C', 'D', 'A', 'B'}
//uniqueInOrder("ABBCcAD")         == {'A', 'B', 'C', 'c', 'A', 'D'}
//uniqueInOrder([1,2,2,3,3])       == {1,2,3}

$iterable = "AAAABBBCCDAABBB";
function uniqueInOrder($iterable){
    var_dump($iterable);
    if($iterable === ""){
        return [];
    }
    if(is_string($iterable)){
        $iterable = str_split($iterable);
    }
    $result=[];
    for($i = 0; $i < count($iterable); $i++){
        if($iterable[$i] !== $iterable[$i+1] ){
            $result[] .= $iterable[$i];
        }
    }
    return $result;
}

//TASK 37 Duplicate Encoder

//The goal of this exercise is to convert a string to a new string
//where each character in the new string is "(" if that character appears only once in the original string,
//or ")" if that character appears more than once in the original string.
//Ignore capitalization when determining if a character is a duplicate.
//Examples
//"din"      =>  "((("
//"recede"   =>  "()()()"
//"Success"  =>  ")())())"
//"(( @"     =>  "))(("

$word = 'Success';
function duplicate_encode($word){
    $word = strtolower($word);
    $word = str_split($word);
    $result='';
    foreach($word as $letter){
        $quantity = 0;
        foreach($word as $val){
            if($letter === $val){
                $quantity++;
            }
        }
        if($quantity < 2){
            $result .= '(';
        }else{
            $result.= ')';
        }
    }
    return $result;
}
echo 'Result of task 37: '.duplicate_encode($word).'<br>';

//TASK38 Meeting

//John has invited some friends. His list is:
//s = "Fred:Corwill;Wilfred:Corwill;Barney:Tornbull;Betty:Tornbull;Bjon:Tornbull;Raphael:Corwill;Alfred:Corwill";
//Could you make a program that
//makes this string uppercase
//gives it sorted in alphabetical order by last name.
//When the last names are the same, sort them by first name.
//Last name and first name of a guest come in the result between parentheses separated by a comma.
//
//So the result of function meeting(s) will be:
//"(CORWILL, ALFRED)(CORWILL, FRED)(CORWILL, RAPHAEL)(CORWILL, WILFRED)(TORNBULL, BARNEY)(TORNBULL, BETTY)(TORNBULL, BJON)"
//It can happen that in two distinct families with the same family name two people have the same first name too.

$s = "Fred:Corwill;Wilfred:Corwill;Barney:Tornbull;Betty:Tornbull;Bjon:Tornbull;Raphael:Corwill;Alfred:Corwill";
function meeting($s) {
    $firstNameAndLastName = explode(";",$s);
    $result=[];
    foreach($firstNameAndLastName as $person){
        list($firstName,$surName) = explode(':',strtoupper($person));
        $result[]="(".$surName.", ".$firstName.")";
    }
    sort($result);
    return implode('',$result);
}
echo 'Result of task 38: '.meeting($s).'<br>';

//TASK 39 unique number in an array

//There is an array with some numbers. All numbers are equal except for one. Try to find it!
//findUniq([ 1, 1, 1, 2, 1, 1 ]) === 2
//findUniq([ 0, 0, 0.55, 0, 0 ]) === 0.55
//It’s guaranteed that array contains at least 3 numbers.

$a = [ 1, 1, 1, 2, 1, 1 ];
function find_uniq($a) {
    sort($a);
    return ($a[0]==$a[1]) ? end($a) : current($a);
}
echo 'Result of task 39: '.find_uniq($a).'<br>';

//TASK 40 The Deaf Rats of Hamelin

//The Pied Piper has been enlisted to play his magical tune and coax all the rats out of town.
//But some of the rats are deaf and are going the wrong way!
//Task
//How many deaf rats are there?
//Legend:
//P = The Pied Piper
//O~ = Rat going left
//~O = Rat going right
//Example
//ex1 ~O~O~O~O P has 0 deaf rats
//ex2 P O~ O~ ~O O~ has 1 deaf rat
//ex3 ~O~O~O~OP~O~OO~ has 2 deaf rats

$town = '~O~O~O~OP~O~OO~';
function countDeafRats($town)
{
    $arr = explode('P',$town);
    $strLeft = str_replace(' ','',$arr[0]);
    $strRight = str_replace(' ','',$arr[1]);
    $arrLeft = str_split($strLeft, 2);
    $arrRight = str_split($strRight, 2);
    $result = 0;
    foreach($arrLeft as $twoLetters){
        if($twoLetters !== '~O' AND $twoLetters !==''){
            $result++;
        }
    }
    foreach($arrRight as $twoLetters){
        if($twoLetters !== 'O~' AND $twoLetters !==''){
            $result++;
        }
    }
    return $result;
}
echo 'Result of task 40: '.countDeafRats($town).'<br>';

//TASK 41 Buying a car

//Let us begin with an example:
//A man has a rather old car being worth $2000.
//He saw a secondhand car being worth $8000. He wants to keep his old car until he can buy the secondhand one.
//
//He thinks he can save $1000 each month but the prices of his old car and of the new one decrease of 1.5 percent per month.
//Furthermore this percent of loss increases of 0.5 percent at the end of every two months.
//Our man finds it difficult to make all these calculations.
//Can you help him?
//How many months will it take him to save up enough money to buy the car he wants, and how much money will he have left over?
//Parameters and return of function:
//parameter (positive int or float, guaranteed) startPriceOld (Old car price)
//parameter (positive int or float, guaranteed) startPriceNew (New car price)
//parameter (positive int or float, guaranteed) savingperMonth
//parameter (positive float or int, guaranteed) percentLossByMonth
//nbMonths(2000, 8000, 1000, 1.5) should return [6, 766] or (6, 766)
//Detail of the above example:
//end month 1: percentLoss 1.5 available -4910.0
//end month 2: percentLoss 2.0 available -3791.7999...
//end month 3: percentLoss 2.0 available -2675.964
//end month 4: percentLoss 2.5 available -1534.06489...
//end month 5: percentLoss 2.5 available -395.71327...
//end month 6: percentLoss 3.0 available 766.158120825...
//return [6, 766] or (6, 766)
//where 6 is the number of months at the end of which he can buy the new car and 766 is the nearest integer to
//766.158... (rounding 766.158 gives 766).
//Note:
//Selling, buying and saving are normally done at end of month.
//Calculations are processed at the end of each considered month but if, by chance from the start, the value of the old car
//is bigger than the value of the new one or equal there is no saving to be made,
// no need to wait so he can at the beginning of the month buy the new car:
//nbMonths(12000, 8000, 1000, 1.5) should return [0, 4000]
//nbMonths(8000, 8000, 1000, 1.5) should return [0, 0]
//We don't take care of a deposit of savings in a bank:-)

    $startPriceOld = 2000;
    $startPriceNew = 8000;
    $savingPerMonth = 1000;
    $percentLossByMonth = 1.5;
function nbMonths($startPriceOld, $startPriceNew, $savingPerMonth, $percentLossByMonth)
{
    $month = 0;
    $currentMoney = $startPriceOld - $startPriceNew;
    $percent = $percentLossByMonth;
    $savedMoney = 0;
    while ($currentMoney < 0) {
        $month++;
        if ($month % 2 == 0) {
            $percent = $percent + 0.5;
        }
        $startPriceOld -= $startPriceOld * $percent / 100;
        $startPriceNew -= $startPriceNew * $percent / 100;
        $savedMoney += $savingPerMonth;
        $currentMoney = $startPriceOld + $savedMoney - $startPriceNew;
    }
    return [$month, round($currentMoney)];
}
    echo 'Result of task 41: months: '.nbMonths($startPriceOld, $startPriceNew, $savingPerMonth, $percentLossByMonth)[0].
    ', remaining money: '.nbMonths($startPriceOld, $startPriceNew, $savingPerMonth, $percentLossByMonth)[1].'<br>';

//TASK 42 Clocky Mc Clock-Face

//Story
//Due to lack of maintenance the minute-hand has fallen off Town Hall clock face.
//And because the local council has lost most of our tax money to a Nigerian email scam
//there are no funds to fix the clock properly.
//Instead, they are asking for volunteer programmers to write some code
//that tell the time by only looking at the remaining hour-hand!
//What a bunch of cheapskates!
//Can you do it?
//Kata
//Given the angle (in degrees) of the hour-hand, return the time in HH:MM format. Round down to the nearest minute.
//Examples
//12:00 = 0 degrees
//03:00 = 90 degrees
//06:00 = 180 degrees
//09:00 = 270 degrees
//12:00 = 360 degrees
//Notes
//0 <= angle <= 360

    $angle = 90;
function what_time_is_it(float $angle): string {
    return gmdate('h:i',$angle * 2 * 60);
}
    echo 'Result of task 42: degrees of the hour-hand : '.$angle.' it is the time: '.what_time_is_it($angle).'<br>';

//TASK 43 Counting Duplicates

//Count the number of Duplicates
//Write a function that will return the count of distinct case-insensitive alphabetic characters
//and numeric digits that occur more than once in the input string.
//The input string can be assumed to contain only alphabets (both uppercase and lowercase) and numeric digits.
//Example
//"abcde" -> 0 # no characters repeats more than once
//"aabbcde" -> 2 # 'a' and 'b'
//"aabBcde" -> 2 # 'a' occurs twice and 'b' twice (`b` and `B`)
//"indivisibility" -> 1 # 'i' occurs six times
//"Indivisibilities" -> 2 # 'i' occurs seven times and 's' occurs twice
//"aA11" -> 2 # 'a' and '1'
//"ABBA" -> 2 # 'A' and 'B' each occur twice

    $text = 'Indivisibilities';
    function duplicateCount($text) {
        $duplicateSigns = 0;
        $signs = array_count_values(str_split(strtolower($text)));
        foreach($signs as $sign){
            if($sign > 1){
                $duplicateSigns++;
            }
        }
        return $duplicateSigns;
    }
    echo 'Result of task 43: '.duplicateCount($text).'<br>';

//TASK 44 Primorial Of a Number

//    Definition (Primorial Of a Number)
//Is similar to factorial of a number, In primorial, not all the natural numbers get multiplied,
//    only prime numbers are multiplied to calculate the primorial of a number.
//    It's denoted with P# and it is the product of the first n prime numbers.
//Task
//Given a number N , calculate its primorial.!alt!alt
//Notes
//Only positive numbers will be passed (N > 0) .
//Input >> Output Examples:
//1- numPrimorial (3) ==> return (30)
//Explanation:
//Since the passed number is (3) ,Then the primorial should obtained by multiplying 2 * 3 * 5 = 30 .
//Mathematically written as , P3# = 30 .
//2- numPrimorial (5) ==> return (2310)
//Explanation:
//Since the passed number is (5) ,Then the primorial should obtained by multiplying 2 * 3 * 5 * 7 * 11 = 2310 .
//Mathematically written as , P5# = 2310 .
//3- numPrimorial (6) ==> return (30030)
//Explanation:
//Since the passed number is (6) ,Then the primorial should obtained by multiplying 2 * 3 * 5 * 7 * 11 * 13 = 30030 .
//Mathematically written as , P6# = 30030 .

    $n = 6;
    function numPrimorial($n) {
        $numbers = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47];
        $arrTruncate = array_slice($numbers,0,$n);
        $result = array_product($arrTruncate);
        return $result;
    }
    echo 'Result of task 44: '.numPrimorial($n).'<br>';

//    TASK 45 Write Number in Expanded Form

//You will be given a number and you will need to return it as a string in Expanded Form. For example:
//
//expanded_form(12); // Should return "10 + 2"
//expanded_form(42); // Should return "40 + 2"
//expanded_form(70304); // Should return "70000 + 300 + 4"
//NOTE: All numbers will be whole numbers greater than 0.

    $n = 12;
    function expanded_form(int $n): string {
        $arr = str_split($n);
        $order = count($arr);
        $result =[];
        foreach ($arr as $number){
            if($number != 0){
                $result[]= $number.str_repeat(0, $order-1);
            }
            $order--;
        }
        return implode(' + ', $result);
    }
    echo 'Result of task 45: '.expanded_form($n).'<br>';

//    TASK 46 Decipher this!

//    You are given a secret message you need to decipher. Here are the things you need to know to decipher it:
//For each word:
//    the second and the last letter is switched (e.g. Hello becomes Holle)
//the first letter is replaced by its character code (e.g. H becomes 72)
//Note: there are no special characters used, only letters and spaces
//Examples
//decipherThis('72olle 103doo 100ya'); // 'Hello good day'
//decipherThis('82yade 115te 103o'); // 'Ready set go'

    $str = '72olle 103doo 100ya';
    function decipherThis($str){
        $sentence = explode(" ", $str);
        $newWords = [];
        $newChar = '';
        $restOfCharacters = '';
        foreach($sentence as $word){
            $characters = str_split($word);
            foreach($characters as $sign){
                if($sign === '0' || $sign === '1' || $sign === '2' || $sign == 3 || $sign == 4 || $sign == 5 || $sign == 6 || $sign == 7 || $sign == 8 || $sign == 9 ){
                    $newChar .= $sign;
                    $newSign = $newChar;
                }else{
                    $restOfCharacters .= $sign;
                }
            }
            $newLetter = chr($newSign);
            $arrRestOfCharacters = str_split($restOfCharacters);
            $first = $arrRestOfCharacters[0];
            $last = end($arrRestOfCharacters);
            $arrRestOfCharacters[0] = $last;
            $arrRestOfCharacters[count($arrRestOfCharacters)-1] = $first;
            $strRestOfCharacters = '';
            $x = count($arrRestOfCharacters);
            for( $i=0 ; $i < $x ; $i++){
                $strRestOfCharacters .=$arrRestOfCharacters[$i];
            }
            $newWords[] = $newLetter.$strRestOfCharacters;
            $newChar = '';
            $restOfCharacters = '';
        }
        return implode(' ',$newWords);
    }
    echo 'Result of task 46: '.decipherThis($str).'<br>';
    
//    TASK 47 New Cashier Does Not Know About Space or Shift

//    Some new cashiers started to work at your restaurant.
//    They are good at taking orders, but they don't know how to capitalize words, or use a space bar!
//All the orders they create look something like this:
//"milkshakepizzachickenfriescokeburgerpizzasandwichmilkshakepizza"
//The kitchen staff are threatening to quit, because of how difficult it is to read the orders.
//Their preference is to get the orders as a nice clean string with spaces and capitals like so:
//"Burger Fries Chicken Pizza Pizza Pizza Sandwich Milkshake Milkshake Coke"
//The kitchen staff expect the items to be in the same order as they appear in the menu.
//The menu items are fairly simple, there is no overlap in the names of the items:
//1. Burger
//2. Fries
//3. Chicken
//4. Pizza
//5. Sandwich
//6. Onionrings
//7. Milkshake
//8. Coke

    $input = 'milkshakepizzachickenfriescokeburgerpizzasandwichmilkshakepizza';
    function getOrder($input){
        $menu =[
            'Burger',
            'Fries',
            'Chicken',
            'Pizza',
            'Sandwich',
            'Onionrings',
            'Milkshake',
            'Coke'
        ];
        $forRelease = '';
        foreach ($menu as $dish){
            $forRelease .= str_repeat($dish . ' ' ,substr_count($input, strtolower($dish)));
        }
        return trim($forRelease);
    }
    echo 'Result of task 47: '.getOrder($input).'<br>';

//    TASK 48 Bleatrix Trotter (The Counting Sheep)

//    Bleatrix Trotter the sheep has devised a strategy that helps her fall asleep faster. First, she picks a number N.
//    Then she starts naming N, 2 × N, 3 × N, and so on.
//Whenever she names a number, she thinks about all of the digits in that number.
//She keeps track of which digits (0, 1, 2, 3, 4, 5, 6, 7, 8, and 9) she has seen at least once so far as part of any number
//she has named. Once she has seen each of the ten digits at least once, she will fall asleep.
//Bleatrix must start with N and must always name (i + 1) × N directly after i × N.
//For example, suppose that Bleatrix picks N = 1692. She would count as follows:
//N = 1692. Now she has seen the digits 1, 2, 6, and 9.
//2N = 3384. Now she has seen the digits 1, 2, 3, 4, 6, 8, and 9.
//3N = 5076. Now she has seen all ten digits, and falls asleep.
//The purpose of this task is to return the last number Bleatrix Trotter sees before falling asleep.
//Input
//Will always be positive integer or zero
//Output
//The last number Bleatrix Trotter sees or "INSOMNIA" (-1 in Rust and C++) if she will count forever

    $n = 1692;
    function trotter($n){
        $collected = [];
        $i = 1;
        while(count(array_unique($collected)) != 10){
            $number = $n * $i;
            $arrNum = str_split($number);
            foreach ($arrNum as $digit){
                array_push($collected, $digit);
            }
            if($i == 100) return "INSOMNIA";
            $i++;
        }
        return $number;
    }
    echo 'Result of task 48: '.trotter($n).'<br>';

//    TASK 49 Selective memory

//    A mad sociopath scientist just came out with a brilliant invention!
//    He extracted his own memories to forget all the people he hates!
//    Now there's a lot of information in there, so he needs your talent as a developer to automatize that task for him.
//You are given the memories as a string containing people's surname and name (comma separated).
//The scientist marked one occurrence of each of the people he hates by putting a '!' right before their name.
//Your task is to destroy all the occurrences of the marked people.
//One more thing ! Hate is contagious, so you also need to erase any memory of the person that comes after any marked name!
//Examples
//Input:
//"Albert Einstein, !Sarah Connor, Marilyn Monroe, Abraham Lincoln, Sarah Connor,
// Sean Connery, Marilyn Monroe, Bjarne Stroustrup, Manson Marilyn, Monroe Mary"
//Output:
//"Albert Einstein, Abraham Lincoln, Sean Connery, Bjarne Stroustrup, Manson Marilyn, Monroe Mary"
//=> We must remove every memories of Sarah Connor because she's marked,
//but as a side-effect we must also remove all the memories about Marilyn Monroe that comes right after her!
//Note that we can't destroy the memories of Manson Marilyn or Monroe Mary, so be careful!

    $memory = 'Albert Einstein, !Sarah Connor, Marilyn Monroe, Abraham Lincoln, Sarah Connor, Sean Connery, Marilyn Monroe, Bjarne Stroustrup, Manson Marilyn, Monroe Mary';
    function select(string $memory) {
        $memory = explode(', ', $memory);
        $memoryToBeDeleted = [];
        foreach ($memory as $firstNameAndLastName =>&$name){
            if(strpos($name,'!') !== false){
                $name = str_replace('!','',$name);
                $memoryToBeDeleted[] = $name;
                $memoryToBeDeleted[] = $memory[$firstNameAndLastName + 1];
            }
        }
        $memoryToBeDeleted = array_unique($memoryToBeDeleted);
        $result = array_diff($memory ,$memoryToBeDeleted);
        return $result = implode(', ',$result);
    }
    echo 'Result of task 49: '.select($memory).'<br>';

//    TASK 50 Write out numbers

//    Create a function that transforms any positive number to a string representing the number in words.
//    The function should work for all numbers between 0 and 999999.
//Examples
//number2words(0)  ==>  "zero"
//number2words(1)  ==>  "one"
//number2words(9)  ==>  "nine"
//number2words(10)  ==>  "ten"
//number2words(17)  ==>  "seventeen"
//number2words(20)  ==>  "twenty"
//number2words(21)  ==>  "twenty-one"
//number2words(45)  ==>  "forty-five"
//number2words(80)  ==>  "eighty"
//number2words(99)  ==>  "ninety-nine"
//number2words(100)  ==>  "one hundred"
//number2words(301)  ==>  "three hundred one"
//number2words(799)  ==>  "seven hundred ninety-nine"
//number2words(800)  ==>  "eight hundred"
//number2words(950)  ==>  "nine hundred fifty"
//number2words(1000)  ==>  "one thousand"
//number2words(1002)  ==>  "one thousand two"
//number2words(3051)  ==>  "three thousand fifty-one"
//number2words(7200)  ==>  "seven thousand two hundred"
//number2words(7219)  ==>  "seven thousand two hundred nineteen"
//number2words(8330)  ==>  "eight thousand three hundred thirty"
//number2words(99999)  ==>  "ninety-nine thousand nine hundred ninety-nine"
//number2words(888888)  ==>  "eight hundred eighty-eight thousand eight hundred eighty-eight"

    $n = 2544;
    function number2words(int $n): string {
        $toNine = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
        $fromTenToNineteen = ['ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];
        $tenths = ['twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];
        if($n < count($toNine) ){
            return $toNine[$n];
        }
        if($n < count($fromTenToNineteen) + count($toNine) ){
            return $fromTenToNineteen[$n - count($toNine)];
        }
        if(strlen((string)$n) < 3){
            return $tenths[$n / 10 - 2] . ($n % 10 === 0 ? '' : '-'.number2words($n % 10));
        }
        if(strlen((string)$n) < 4){
            return number2words(floor($n / 100)) . ' hundred' . ($n % 100 ? ' '.number2words($n % 100) : '');
        }
        return number2words(floor($n / 1000)).' thousand'.($n % 1000 ? ' '.number2words($n % 1000) : '');
    }
    echo 'Result of task 50: '.number2words($n).'<br>';

//    TASK 51 Array Deep Count

//    count() will give you the number of top-level elements in an array
//    if exactly one argument $a is passed in which is the array.
//Your task is to create a function deepCount that returns the number of ALL elements within an array,
//        including any within inner-level arrays.
//For example:
//deep_c([1, 2, 3]);
////>>>>> 3
//deep_c(["x", "y", ["z"]]);
////>>>>> 4
//deep_c([1, 2, [3, 4, [5]]]);
////>>>>> 7
//The input will always be an array.
//In PHP you may not assume that the array passed in will be non-associative.
//    Please note that count(), eval() and COUNT_RECURSIVE are disallowed - you should be able to implement
//    the logic for deep_c() yourself ;)

    $a= ["x", "y", ["z"]];
    function deep_c(array $a): int {
        $result = 0;
        if (empty($a)){
            return $result;
        }
        foreach($a as $val){
            $result++;
            if(is_array($val)){
                $result += deep_c($val);
            }
        }
        return $result;
    }
    echo 'Result of task 51: '.deep_c($a);